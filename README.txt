Instalacja i uruchamianie

Wymagania to "nixowy system z powłoką typu bash oraz menedżer pakietów pythonowych `pip`. Zalecany sposób instalacji przy użyciu virtualenv (nie ma takiej konieczności).

git clone https://marekbrzoska@bitbucket.org/marekbrzoska/mgr-projekt.git
cd mgr-projekt/
pip install -e git+https://github.com/mbad/kitabu.git#egg=django_kitabu-dev
./manage.py runserver

Aplikacja z prekonfigurowaną bazą danych będzie dostępna w przeglądarce pod adresem http://127.0.0.1:8000.
