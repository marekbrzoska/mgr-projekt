from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', 'saloon.views.home', name='home'),
    url(r'^fryzjerzy/$', 'saloon.views.hairdressers', name='hairdressers'),
    url(r'^fryzjer/(\d+)/$', 'saloon.views.hairdresser', name='hairdresser'),
    url(r'^fryzjer/(\d+)/najblizsze wolne terminy/$', 'saloon.views.hairdresser_availability', name='hairdresser_availability'),
    url(r'^rezerwuj wizyte/(\d+)/([0-9-]+)/([0-9:]+)/(\d+)/$', 'saloon.views.reserve', name='reserve'),
    url(r'^szczegoly wizyty/(\d+)/$', 'saloon.views.visit', name='visit'),
    url(r'^about$', 'saloon.views.about', name='about'),

    url(r'^admin/', include(admin.site.urls)),
)
