#-*- coding=utf-8 -*-

import datetime

from django.db import models

from kitabu.models import subjects, clusters, reservations, validators


class Saloon(clusters.BaseCluster):
    def __unicode__(self):
        return self.name


class HairDresser(
    subjects.SubjectWithApprovableReservations,
    subjects.ExclusiveSubjectMixin,
    subjects.BaseSubject
):
    short_name = models.CharField(max_length=20)
    full_name = models.CharField(max_length=50)
    cluster = models.ForeignKey(Saloon, related_name='subjects')

    def __unicode__(self):
        return self.short_name

    def reserve(self, **kwargs):
        start = kwargs.get('start')
        duration = kwargs.pop('duration')
        end = start + datetime.timedelta(minutes=duration)
        kwargs['end'] = end
        return super(HairDresser, self).reserve(**kwargs)


class WorkingDays(validators.PeriodsInWeekdaysValidator):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name


class WorkingHours(validators.WithinDayPeriod):
    working_day = models.ForeignKey(WorkingDays, related_name='periods')


class Visit(reservations.BaseReservation, reservations.ApprovableReservation):
    subject = models.ForeignKey(HairDresser, related_name='reservations')
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=20)


class TimeIntervalValidator(validators.TimeIntervalValidator):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name



class NotWithinPeriodValidator(validators.NotWithinPeriodValidator):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name
