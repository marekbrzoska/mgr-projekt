from django.contrib import admin
from . import models


class HairDresserAdmin(admin.ModelAdmin):
    list_filter = ('cluster__name',)

admin.site.register(models.HairDresser, HairDresserAdmin)

admin.site.register(models.Visit)

admin.site.register(models.Saloon)

admin.site.register(models.TimeIntervalValidator)

admin.site.register(models.NotWithinPeriodValidator)


class WorkingHoursAdmin(admin.TabularInline):
    model = models.WorkingHours


class WorkingDaysAdmin(admin.ModelAdmin):
    inlines = [WorkingHoursAdmin]


admin.site.register(models.WorkingDays, WorkingDaysAdmin)
