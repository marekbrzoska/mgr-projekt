#-*- coding=utf-8 -*-

from django import forms
from django.forms.widgets import DateInput, HiddenInput


DURATION_CHOICES = [(15, u'Konsultacje'),
                    (30, u'Podcinanie końcówek'),
                    (45, u'Strzyżenie podstawowe'),
                    (60, u'Modelowanie'),
                    (120, u'Farbowanie'),
                    (150, u'Trwała')]


class PeriodForm(forms.Form):
    date = forms.DateField(label=u'Dzień', widget=DateInput(attrs={'class': 'datepicker'}), input_formats=["%Y-%m-%d"])
    time = forms.TimeField(label=u'Godzina', widget=DateInput(attrs={'class': 'timepicker'}))
    duration = forms.ChoiceField(label=u'Usługa', choices=DURATION_CHOICES)


class ConfirmationForm(forms.Form):
    name = forms.CharField(max_length=100)
    phone = forms.CharField(max_length=20)
    visit_id = forms.IntegerField(widget=HiddenInput)


class SearchPeriodForm(forms.Form):
    duration = forms.ChoiceField(label=u'Usługa', choices=DURATION_CHOICES)
