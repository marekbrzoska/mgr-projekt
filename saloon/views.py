#-*- coding=utf-8 -*-

import datetime

from saloon.forms import PeriodForm, ConfirmationForm, SearchPeriodForm, DURATION_CHOICES

# import urllib
#
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
#
# from kitabu.search.available import Clusters as ClustersSearcher, Subjects as SubjectSearcher, FindPeriod
# from kitabu.search.reservations import SingleSubjectManagerReservationSearch
# from lanes.models import Lane, LaneReservation
# from models import Pool
# from forms import PoolReservationsSearchForm, ClusterSearchForm, PeriodSearchForm
#
#
#
# cluster_searcher = ClustersSearcher(subject_model=Lane, cluster_model=Pool)

from saloon.models import HairDresser, Visit

from kitabu.search.available import ExclusivelyAvailableSubjects as HairdresserSearcher, FindPeriod
from kitabu.exceptions import ReservationError, ReservationValidationError


def home(request):
    form = PeriodForm(request.GET or None)
    results = []

    if form.is_valid():
        start_date = form.cleaned_data['date']
        start_time = form.cleaned_data['time']
        start = datetime.datetime.combine(start_date, start_time)
        end = start + datetime.timedelta(minutes=int(form.cleaned_data['duration']))
        search = HairdresserSearcher(HairDresser).valid_search
        results = search(start=start, end=end)

    return render(
        request,
        'index.html',
        {
            'form': form,
            'results': results,
        }
    )


def reserve(request, hairdresser_id, date, time, duration):

    hairdresser = get_object_or_404(HairDresser, pk=hairdresser_id)
    service = dict(DURATION_CHOICES)[int(duration)]
    problem_msg = None
    start = datetime.datetime.strptime(' '.join([date, time]), '%Y-%m-%d %H:%M')

    if request.POST:
        confirmation_form = ConfirmationForm(request.POST)
        if confirmation_form.is_valid():
            try:
                visit = Visit.objects.get(pk=confirmation_form.cleaned_data['visit_id'])
            except Visit.DoesNotExist:
                visit, problem_msg = try_reserve(hairdresser, start, duration)
            if not visit:
                return redirect('.')
            visit.name = confirmation_form.cleaned_data['name']
            visit.phone = confirmation_form.cleaned_data['phone']
            visit.approved = True
            visit.save()
            return redirect(reverse('visit', args=[visit.id]))
        else:
            try:
                visit = Visit.objects.get(pk=int(confirmation_form.data['visit_id']))
            except Visit.DoesNotExist:
                return redirect('.')
    else:
        visit, problem_msg = try_reserve(hairdresser, start, duration)
        if visit:
            confirmation_form = ConfirmationForm(initial={'visit_id': visit.id})
        else:
            confirmation_form = None

    return render(
        request,
        'reserve.html',
        {
            'hairdresser': hairdresser,
            'date': date,
            'time': time,
            'duration': duration,
            'service': service,
            'problem': problem_msg,
            'visit': visit,
            'confirmation_form': confirmation_form,
        }
    )


def try_reserve(hairdresser, start, duration):
    try:
        return hairdresser.reserve(start=start, duration=int(duration)), None
    except ReservationError as e:
        return None, e.message


def visit(request, visit_id):
    visit = get_object_or_404(Visit, pk=visit_id)
    return render(request, 'visit.html', {'visit': visit})


def hairdressers(request):
    return render(
        request,
        'hairdressers.html',
        {'hairdressers': HairDresser.objects.all()}
    )


def hairdresser(request, hairdresser_id):
    hairdresser = get_object_or_404(HairDresser, pk=hairdresser_id)

    return render(
        request,
        'hairdresser.html',
        {
            'hairdresser': hairdresser,
            'timeline': hairdresser.reservations.filter(start__gt=datetime.datetime.now()).order_by('start'),
            'search_periods_form': SearchPeriodForm(),
        }
    )


def hairdresser_availability(request, hairdresser_id):
    hairdresser = get_object_or_404(HairDresser, pk=hairdresser_id)
    form = SearchPeriodForm(request.GET)
    assert form.is_valid()
    duration = form.cleaned_data['duration']
    soonest_possible_reservations = find_upcoming_possibilities(
        duration, hairdresser)

    return render(
        request,
        'hairdresser.html',
        {
            'hairdresser': hairdresser,
            'upcoming': soonest_possible_reservations,
            'duration': duration,
        }
    )


def find_upcoming_possibilities(duration, hairdresser):
    duration = datetime.timedelta(minutes=int(duration))
    now = datetime.datetime.now()
    now = now.replace(microsecond=0, second=0, minute=now.minute - now.minute % 15)
    step = datetime.timedelta(minutes=15)

    in_a_year = now.replace(year=now.year + 1)
    long_periods = FindPeriod().search(
        start=now,
        end=in_a_year,
        required_duration=duration,
        subject=hairdresser)
    found = 0
    for long_period_start, long_period_end in long_periods:
        start = long_period_start
        while found < 10 and start + duration <= long_period_end:
            reservation = Visit(subject=hairdresser, start=start, end=start + duration)
            try:
                hairdresser._validate_reservation(reservation)
                hairdresser._validate_exclusive(reservation)  # TODO: delete
            except ReservationValidationError:
                pass
            else:
                yield start
                found += 1
            start += step


def about(request):
    return render(request, 'about.html')
